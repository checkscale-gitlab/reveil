// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.Collections.Generic;
using Reveil.Backend.Models.Configuration.Authentication;

namespace Reveil.Backend.Models.Configuration {
  public class AuthenticationConfiguration {
    public IList<AuthenticationProviderConfigurationBase> Providers { get; set; } = new List<AuthenticationProviderConfigurationBase>();
  }
}
