using System.Net;
using CSharpFunctionalExtensions;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.BusinessLogic {
  public class ContentBl : IContentBl {
    private readonly IGenericRepository<ContentEntity> _repository;

    public ContentBl(IGenericRepository<ContentEntity> repository) {
      _repository = repository;
    }

    public Result<ContentEntryResponse, ErrorResponse> GetContent(string contentType, string languageCode) =>
      _repository
        .Get($"{contentType}-{languageCode}")
        .OrServerError()
        .MapValueOrNotFound()
        .Map(e => ContentEntryResponse.Create(e, HttpStatusCode.OK, "Content found"));

    public Result<ContentEntryResponse, ErrorResponse> AddOrUpdateContent(string contentType, string languageCode, string content) {
      var contentEntity = new ContentEntity {
        Id = $"{contentType}-{languageCode}",
        Content = content,
        Type = contentType,
        Language = languageCode
      };

      var existingRepoResult = _repository.Get(contentEntity.Id);
      if (existingRepoResult.IsFailure) return ErrorResponse.Create(HttpStatusCode.InternalServerError, existingRepoResult.Error);

      if (existingRepoResult.Value.HasNoValue) {
        _repository.Add(contentEntity);
        return ContentEntryResponse.Create(contentEntity, HttpStatusCode.Created, "Created Content");
      }

      _repository.Remove(contentEntity.Id);
      _repository.Add(contentEntity);
      return ContentEntryResponse.Create(contentEntity, HttpStatusCode.Created, "Updated Content");
    }
  }
}
