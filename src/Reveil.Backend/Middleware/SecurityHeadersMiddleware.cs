using Microsoft.AspNetCore.Builder;

namespace Reveil.Backend.Middleware {
  public static class SecurityHeadersMiddleware {
    public static IApplicationBuilder UseSecurityHeaders(this IApplicationBuilder app, ContentSecurityPolicyCollector policyCollector) {
      var contentSecurityPolicyValue = policyCollector.Build();
      app.Use(async (context, next) => {
        context.Response.Headers.Add("X-Frame-Options", "DENY");
        context.Response.Headers.Add("X-Xss-Protection", "1; mode=block");
        context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
        context.Response.Headers.Add("Referrer-Policy", "no-referrer");
        context.Response.Headers.Add("X-Permitted-Cross-Domain-Policies", "none");
        context.Response.Headers.Add("Permissions-Policy",
          "accelerometer=(), camera=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()");

        if (context.Request.Path.Value?.Contains("/swagger/index.html") ?? false) {
          await next();
          return;
        }

        context.Response.Headers.Add("Content-Security-Policy", contentSecurityPolicyValue);
        await next();
      });
      return app;
    }
  }
}
