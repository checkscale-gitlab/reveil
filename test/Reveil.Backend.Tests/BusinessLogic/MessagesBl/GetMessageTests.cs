using System.Net;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.MessagesBl {
  public class GetMessageTests {
    private readonly INotificationSenderFactory _devNullSenderFactory;
    public GetMessageTests() {
      _devNullSenderFactory = new NotificationSenderFactory(Options.Create(new NotificationConfiguration()));
    }
    
    [Theory]
    [InlineData("0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestNotExistingCaseReturns404ErrorResponse(string caseId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      var r = bl.GetMessage(caseId, caseId);
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestGetNotExistingMessageForExistingCaseReturns404Response(string caseId, string messageId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      cases.Add(new CaseEntity {Id = caseId});
      var r = bl.GetMessage(caseId, messageId);
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestGetExistingMessageForExistingCaseReturns200Response(string caseId, string messageId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      cases.Add(new CaseEntity {Id = caseId});
      messages.Add(new MessageEntity {CaseId = caseId, Id = messageId, Data = ""});
      var r = bl.GetMessage(caseId, messageId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<MessageResponse>();
      r.Value.Data.Should().Be("");
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Id.Should().Be(messageId);
      r.Value.CaseId.Should().Be(caseId);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }
  }
}
