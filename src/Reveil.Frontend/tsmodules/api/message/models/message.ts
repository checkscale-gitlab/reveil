import * as api from '~/api'

import { IModelMessagePayload } from './payload'
import { IModelFile } from '~/tsmodules/api/file/models'
import { IModelMgmt } from '~/tsmodules/api/mgmt/models'

export interface IModelMessage {
    modeltype: string,
    
    response: api.MessageResponse,
    payload: IModelMessagePayload,

    computed_author: IModelMgmt | null,
    computed_files: Array<IModelFile>,
}
