using System.Net;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Requests;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Structures;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.MessagesBl {
  public class CreateMessageTests {
    private readonly INotificationSenderFactory _devNullSenderFactory;
    public CreateMessageTests() {
      _devNullSenderFactory = new NotificationSenderFactory(Options.Create(new NotificationConfiguration()));
    }
    
    [Theory]
    [InlineData("0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestCreateMessageForNotExistingCaseReturns404ErrorResponse(string caseId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      var r = bl.CreateMessage(caseId, caseId, new CreateMessageRequest {Data = ""});
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestCreateAlreadyExistingMessageReturns409ErrorResponse(string caseId, string messageId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      cases.Add(new CaseEntity {Id = caseId});
      messages.Add(new MessageEntity {CaseId = caseId, Id = messageId, Data = ""});

      var r = bl.CreateMessage(caseId, messageId, new CreateMessageRequest {Data = ""});
      r.IsSuccess.Should().BeFalse();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.Conflict);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestCreateNewMessageAddsEntryToRepoAddsJournalAndReturns201CreatedResponse(string caseId, string messageId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var messages = RepositoryFactory.Create<MessageEntity>(repoType);
      var files = RepositoryFactory.Create<FileEntity>(repoType);
      var bl = new Backend.BusinessLogic.MessagesBl(cases, journals, messages, files, _devNullSenderFactory);
      cases.Add(new CaseEntity {Id = caseId});
      var r = bl.CreateMessage(caseId, messageId, new CreateMessageRequest {Data = ""});

      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<SuccessResponse>();
      r.Value.StatusCode.Should().Be(HttpStatusCode.Created);
      messages.Get(messageId).Value.GetValueOrDefault().Id.Should().Be(messageId);
      journals.GetAll(c => c.CaseId == caseId).Value
        .Should()
        .ContainSingle(c => c.JournalType == JournalEntryType.NewMessageByCreator || c.JournalType == JournalEntryType.NewMessageByManagement);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
      messages.Remove(_ => true);
      files.Remove(_ => true);
    }
  }
}
