using System;
using System.Linq;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Middleware.Filter {
  public class RequireAuthorizationAttribute : TypeFilterAttribute {
    public RequireAuthorizationAttribute()
      : base(typeof(AuthorizationFilter)) {
    }
  }

  public class AuthorizationFilter : IActionFilter {
    private readonly IGenericRepository<AuthorizationEntity> _authorizationTokenRepository;
    private readonly IGenericRepository<CaseEntity> _caseRepository;

    public AuthorizationFilter(
      IGenericRepository<AuthorizationEntity> authorizationTokenRepository,
      IGenericRepository<CaseEntity> caseRepository) {
      _authorizationTokenRepository = authorizationTokenRepository;
      _caseRepository = caseRepository;
    }

    public void OnActionExecuting(ActionExecutingContext context) {
      if (!context.HttpContext.Request.Headers.ContainsKey("Bearer")) {
        throw new UnauthorizedAccessException();
      }

      var authHeader = context.HttpContext.Request.Headers["Bearer"]
        .First()
        .Replace("Bearer= ", "")
        .Replace("Bearer: ", "");

      var authResult = GetCaseId(context)
        .ToResult("Could not get CaseId from Request")
        .Bind(caseId => _caseRepository.Get(caseId))
        .Ensure(c => c.HasNoValue || c.GetValueOrDefault().AuthorizationToken == authHeader, "Whistleblower Unauthorized")
        .Map(c => true)
        .OnFailureCompensate(_ => _authorizationTokenRepository.Get(h => h.AuthorizationToken == authHeader).Map(a => a.HasValue));

      if (authResult.IsFailure)
        throw new UnauthorizedAccessException(authResult.Error);
      if (authResult.IsSuccess && !authResult.Value)
        throw new UnauthorizedAccessException("Your AuthorizationToken does not grant you the required permissions for this operation.");
    }

    private static Maybe<string> GetCaseId(ActionExecutingContext context) {
      try {
        context.ActionArguments.TryGetValue("caseId", out var caseId);
        var caseIdString = (string)caseId;
        return Maybe<string>.From(caseIdString);
      } catch {
        return Maybe<string>.None;
      }
    }

    public void OnActionExecuted(ActionExecutedContext context) {
    }
  }
}
