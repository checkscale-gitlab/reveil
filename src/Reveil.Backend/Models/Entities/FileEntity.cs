using System;

namespace Reveil.Backend.Models.Entities {
  public class FileEntity : BaseEntity {
    public string CaseId { get; set; }
    public string MessageId { get; set; }
    public string UserId { get; set; }
    public string FileName { get; set; }
    public int DataProcessingMethodHint { get; set; }
    public string Data { get; set; }
    public DateTime UploadDate { get; set; }
  }
}
