namespace Reveil.Backend.Models.Configuration.Authentication {
  public class AuthenticationProviderConfigurationBase {
    public string Type { get; set; }
  }
}
