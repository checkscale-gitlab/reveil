import * as api from '~/api'

export interface IModelCaseResponse {
    response: api.SuccessResponse | api.ErrorResponse
    ref_obj: string,
}
