export default async (context, locale) => {
    return await Promise.resolve({
        // inline :: editor
        genericinlineeditor__subject: 'Betreff',
        genericinlineeditor__upload: 'Files',
        genericinlineeditor__submit: 'Send',

        // assets :: image
        assetsimageset__upload: 'File',
        assetsimageset__submit: 'Upload',
        assetsimageset__status_upload: '... uploading',
        assetsimageset__status_success: 'erfolgreich!',
        assetsimageset__status_failed: 'nicht erfolgreich!',

        // legal :: assets
        assets_eurlex_pdf: 'https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:32019L1937',

        // comps :: loginmgmt
        loginmgmt_label_username: 'Benutzer',
        loginmgmt_label_password: 'Kennwort',
        loginmgmt_label_login: 'Anmelden',
        loginmgmt_rules_username_required: 'Feld verplichtend.',
        loginmgmt_rules_password_required: 'Feld verplichtend.',
        loginmgmt_rules_password_length: 'Min. {{ num_chars }} Zeichen.',
        loginmgmt_error_unauthorized: 'Die eingegebenen Zugangsdaten sind fehlerhaft.',

        // comps :: loginuser
        loginuserec_label_caseid: 'Meldungsreferenz',
        loginuserec_label_password: 'Kennwort',
        loginuserec_label_login: 'Anmelden',
        loginuserec_rules_caseid_required: 'Feld verpflichtend.',
        loginuserec_rules_caseid_length: 'Min. {{ num_chars }} Zeichen.',
        loginuserec_rules_password_required: 'Feld verpflichtend.',
        loginuserec_rules_password_length: 'Min. {{ num_chars }} Zeichen.',
        loginuserec_error_login: 'Anmeldung gescheitert. Bitte prüfen Sie die Zugangsdaten.',

        loginusernc_title: 'Anmeldung neue Meldung',
        loginusernc_label_password: 'Kennwort',
        loginusernc_label_passwordrepeat: 'Kennwort wiederholen',
        loginusernc_label_consent: 'Zustimmung zur',
        loginusernc_label_consentref: 'Datenverarbeitung',
        loginusernc_label_login: 'Anmelden',
        loginusernc_rules_password_required: 'Feld verpflichtend.',
        loginusernc_rules_password_length: 'Min. {{ num_chars }} Zeichen.',
        loginusernc_rules_password_match: 'Kennwörter stimmen nicht überein.',
        loginusernc_rules_consent_accept: 'Zustimmung verpflichtend.',
        loginusernc_error_login: '',
        

        // comps :: assetsimageset
        assetsimageset_title: 'Upload',
        assetsimageset_status_upload: 'sende Daten ...',
        assetsimageset_status_success: 'senden erfolgreich',
        assetsimageset_status_failed: 'senden fehlgeschlagen',
        assetsimageset_label_upload: 'Upload',

        // comps :: assetstextset
        assetstextset_title: 'Text',
        assetstextset_label_text: 'Text',
        assetstextset_label_submit: 'Speichern',
        assetstextset_status_upload: 'sende Daten ...',
        assetstextset_status_success: 'senden erfolgreich',
        assetstextset_status_failed: 'senden fehlgeschlagen',

        // comps :: assetstextset
        assetscolorset_title: 'Text',
        assetscolorset_label_text: 'Text',
        assetscolorset_label_submit: 'Speichern',
        assetscolorset_status_upload: 'sende Daten ...',
        assetscolorset_status_success: 'senden erfolgreich',
        assetscolorset_status_failed: 'senden fehlgeschlagen',

        // comps :: inlinelanguage
        inlinelanguage_label_language: 'Sprache',

        // comps :: inlineqrcode
        inlineqrcode_label_clipboard: 'Kopiere in Zwischenspeicher',

        // comps :: dialog
        dialog_button_messages: 'Nachricht',
        dialog_button_journals: 'Journal',
        dialogjournal_category_user: 'Benutzer',
        dialogjournal_category_mgmt: 'Management',
        dialogjournal_category_case: 'System',
        dialogjournal_category_auth: 'Authorisierung',
        dialogjournal_category_info: 'Information',
        dialogjournal_message_user_101: 'Sachverhalt geschlossen durch Benutzer.',
        dialogjournal_message_user_102: 'Neue Nachricht von Benutzer.',
        dialogjournal_message_mgmt_201: 'Sachverhalt geschlossen durch Management.',
        dialogjournal_message_mgmt_202: 'Neue Nachricht von {{ mgmt.name }}.',
        dialogjournal_message_case_301: 'Sachverhalt eröffnet. Meldungsreferenz: {{ case.id }}',
        dialogjournal_message_case_302: 'Sachverhalt abgelehnt.',
        dialogjournal_message_case_303: 'Sachverhalt abgeschlossen.',
        dialogmessage_messagefrom_mgmt: 'Nachricht von {{ mgmt.name }}',
        dialogmessage_messagefrom_self: 'Nachricht von dir',
        dialogmessage_messagefrom_anon: 'Nachricht von anon. Benutzer',

        // comps :: caselist
        caselistelement_subject: 'Betreff',
        caselistelement_creationtime: 'Erstellt',
        caselistelement_activitytime: 'Aktivität',
        // caselistelement_messagefrom_mgmt: 'Nachricht von {{ mgmt.name }}',
        // caselistelement_messagefrom_self: 'Nachricht von dir',
        caselistelement_messagefrom_anon: 'anon. Benutzer',
        caselistnoelement_label_empty: 'no Message',

        // comps :: caseedit
        caseedit_label_caseisnew_head: 'Erstelle hier hier neue Meldung.',
        caseedit_label_caseisnew_body: 'Hier kannst du mit uns vollständig anonym im Rahmen der Whistleblower-Richtline Kontakt aufnehmen.',
        caseedit_label_savecaseref: 'Meldungsreferenz ist automatisch generiert. Für den erneuten Zugriff auf den Sachverhalt wird diese benötigt.',
        caseeditqrcode_label_clipboard: 'Kopieren in Zwischenablage',

        // layout :: *
        layoutnavbar_label_login: 'Anmeldung',
        layoutnavbar_label_logout: 'Abmeldung',
        layoutnavbar_label_userdashboard: 'Übersicht',
        layoutnavbar_label_mgmtdashboard: 'Verwaltung',
        layoutfooter_label_impressum: 'Impressum',
        layoutfooter_label_consent: 'Zustimmung',
        layoutfooter_label_aboutus: 'Über uns',
        
        layouterror_label_headline1: 'Da ging was schief.',
        layouterror_label_headline2: '... bitte probiere es später erneut.',
        layouterror_label_linkback: 'zurück zur Startseite',

        // page :: loginuser
        ploginuser_label_nc: 'Anmeldung - neue Meldung',
        ploginuser_label_ec: 'Anmeldung - bestehende Meldung',
        ploginuser_infobox_head: 'Nutzer-Hinweis',
        ploginuser_infobox_body: 'Informiere uns vollständig anonym. Keine Registrierung notwendig.',

        // page :: mgmt-dashboard
        pmgmtdashboard_caselist: 'Meldungen',
        pmgmtdashboard_startpage: 'Startseite',
        pmgmtdashboard_consent: 'Zustimmung',
        pmgmtdashboard_impressum: 'Impressum',
        pmgmtdashboard_aboutus: 'Über uns',
        pmgmtdashboard_customization: 'Personalisierung',
        pmgmtdashboard_cases: 'Meldungen',
        pmgmtdashboard_contents: 'Inhalte',
        pmgmtdashboard_settings: 'Einstellungen',

        // page :: mgmt-settings
        pmgmtsettings_label_title1: 'Titel 1. Zeile',
        pmgmtsettings_label_title2: 'Titel 2. Zeile',
        pmgmtsettings_label_logolarge: 'Logo (groß)',

        // comps :: sitenav
        sitenav_label_startpage: 'Startseite',
        sitenav_label_content: 'Inhalte',
        sitenav_label_consent: 'Zustimmung',
        sitenav_label_aboutus: 'Über uns',
        sitenav_label_impressum: 'Impressum',
        sitenav_label_user: 'Benutzer',
        sitenav_label_mgmt: 'Verwaltung',
        sitenav_label_case: 'Meldung',
        sitenav_label_caseshow: 'Detailansicht',
        sitenav_label_caselist: 'Listenansicht',
        sitenav_label_customization: 'Personalisierung',
        sitenav_label_settings: 'Einstellungen',

        // eventlist
        casestate_label_isopen: '',
        casestate_label_isclosed: '',
        casestate_label_isarchived: '', 

        // comps :: generic cicle
        loadingcircle_loading: 'Lade Inhalte ...',

        // comps :: loading process
        loadingprocess_upload: 'Lade',
        loadingprocess_upload_item: 'Element',
    })
}
