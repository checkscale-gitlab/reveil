using System.Net;
using Microsoft.AspNetCore.Mvc;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Middleware.Filter;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Controllers {
  [ApiController]
  [Route("[controller]")]
  [Produces("application/json")]
  [Consumes("application/json")]
  public class JournalController : ControllerBase {
    private readonly IJournalBl _businessLogic;

    public JournalController(IJournalBl businessLogic) {
      _businessLogic = businessLogic;
    }

    /// <summary>
    /// Get all journal entries for a case
    /// </summary>
    /// <param name="caseId">the Id of the case</param>
    /// <returns></returns>
    [HttpGet("/cases/{caseId}/journal", Name = nameof(GetJournalEntriesForCase))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(ChildrenListResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetJournalEntriesForCase(string caseId) => _businessLogic.GetJournalEntriesByCase(caseId).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Gets the specific journal entry
    /// </summary>
    /// <param name="caseId">the case the journal belongs to</param>
    /// <param name="journalId"></param>
    /// <returns></returns>
    [HttpGet("/cases/{caseId}/journal/{journalId}", Name = nameof(GetJournalEntry))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(JournalEntryResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetJournalEntry(string caseId, string journalId) =>
      _businessLogic.GetJournalEntry(caseId, journalId).MatchAndSetStatusCode(HttpContext);
  }
}
