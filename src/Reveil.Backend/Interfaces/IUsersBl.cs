using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Interfaces {
  public interface IUsersBl {
    Task<Result<UserResponse, ErrorResponse>> GetUser(string userId);
  }
}
