module.exports = {
  preset: '@nuxt/test-utils',

  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js'
  },

  testTimeout: 60000,

  collectCoverage: false,
  collectCoverageFrom: [
    '<rootDir>/tsmodules/**',
  ],

  testEnvironment: 'jsdom',
}
