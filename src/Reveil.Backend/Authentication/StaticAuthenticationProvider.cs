using System;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models;
using Reveil.Backend.Models.Configuration.Authentication;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Authentication {
  public class StaticAuthenticationProvider : IAuthenticationProvider {
    private readonly StaticAuthenticationProviderConfiguration _configuration;

    public StaticAuthenticationProvider(StaticAuthenticationProviderConfiguration configuration) {
      _configuration = configuration;
    }

    public Task<Result<Maybe<UserEntity>>> GetUser(string userId) {
      var user = _configuration.Administrators.FirstOrDefault(a => a.UserId == userId);
      return Task.FromResult(user == null
        ? Result.Success(Maybe<UserEntity>.None)
        : Result.Success(Maybe<UserEntity>.From(Map(user))));
    }

    public Task<Result<bool>> AuthenticateAsync(UserEntry entry) {
      try {
        var r = _configuration.Administrators
          .Any(c => c.Username.Equals(entry.Username) && c.Password.Equals(entry.Password));
        return Task.FromResult(Result.Success(r));
      } catch (Exception exc) {
        return Task.FromResult(Result.Failure<bool>(exc.Message));
      }
    }

    private static UserEntity Map(UserEntry entry) {
      return new UserEntity {
        Firstname = entry.FirstName,
        LastName = entry.LastName,
        Mail = entry.UserMail,
        Id = entry.UserId,
        Phone = entry.PhoneNumber ?? "Unknown",
        DisplayName = entry.DisplayName
      };
    }
  }
}
