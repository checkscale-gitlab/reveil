import { IModelAuthBase } from './base'

export interface IModelAuthUser extends IModelAuthBase {
    computed_token: string,
    computed_case_id: string,
}
