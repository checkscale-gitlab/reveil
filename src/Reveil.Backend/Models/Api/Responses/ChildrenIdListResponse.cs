// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.Collections.Generic;
using System.Linq;
using System.Net;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Models.Api.Responses {
  public class ChildrenListResponse : SuccessResponse {
    public List<string> Ids { get; set; }
    public int Count => Ids?.Count ?? 0;

    public static ChildrenListResponse Create(IEnumerable<string> ids, HttpStatusCode statusCode, string message = null) {
      var r = Create(statusCode, message);
      return new ChildrenListResponse {
        Ids = ids.ToList(),
        Message = r.Message,
        DateTime = r.DateTime,
        IsSuccess = r.IsSuccess,
        StatusCode = r.StatusCode
      };
    }
  }
}
