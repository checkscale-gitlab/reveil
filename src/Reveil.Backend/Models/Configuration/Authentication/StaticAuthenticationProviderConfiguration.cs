// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.Collections.Generic;

namespace Reveil.Backend.Models.Configuration.Authentication {
  public class StaticAuthenticationProviderConfiguration : AuthenticationProviderConfigurationBase {
    public List<UserEntry> Administrators { get; set; }
  }
}
