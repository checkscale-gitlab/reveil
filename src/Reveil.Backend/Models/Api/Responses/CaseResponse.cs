// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;
using System.Collections.Generic;
using System.Net;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Api.Responses.Primitives;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Structures;

namespace Reveil.Backend.Models.Api.Responses {
  public class CaseResponse : SuccessResponse {
    public string CaseId { get; set; }
    public CaseState CaseState { get; set; }
    public DateTime CreationDate { get; set; }

    public List<AddressableId> Messages { get; set; }
    public List<AddressableId> Journals { get; set; }

    public static CaseResponse Create(CaseEntity e, List<AddressableId> messageIds, List<AddressableId> journalIds, HttpStatusCode statusCode,
      string message = null) {
      var r = Create(statusCode, message);
      return new CaseResponse {
        CaseId = e.Id,
        CaseState = e.State,
        CreationDate = e.CreationDate,
        Messages = messageIds,
        Journals = journalIds,
        Message = r.Message,
        DateTime = r.DateTime,
        IsSuccess = r.IsSuccess,
        StatusCode = r.StatusCode
      };
    }
  }
}
