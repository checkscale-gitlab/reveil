using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;

namespace Reveil.Backend.Models.Configuration.Notification {
  public static class NotificationConfigurationLogic {
    public static void ConfigureNotificationConfiguration(IConfiguration configuration, NotificationConfiguration opts) {
      var configuredTargetConfigurations = new List<NotificationTargetConfigurationBase>();
      var availableTargetConfigurationTypes = GetAvailableNotificationTargetConfigurationTypes();

      for (var index = 0;; index++) {
        var targetConfigurationPathWithIndex = $"Notification:Targets:{index}";
        var userConfiguredTargetType = configuration.GetValue<string>($"{targetConfigurationPathWithIndex}:Type");
        if (userConfiguredTargetType == null) // we reached the end. no further Providers configured
          break;

        var correspondingProviderType = availableTargetConfigurationTypes.GetNotificationTargetConfigurationTypeByName(userConfiguredTargetType);
        if (correspondingProviderType == null) break;

        var t = (NotificationTargetConfigurationBase)configuration
          .GetSection(targetConfigurationPathWithIndex)
          .Get(correspondingProviderType);
        configuredTargetConfigurations.Add(t);
      }

      opts.Targets = configuredTargetConfigurations;
    }
    
    public static List<Type> GetAvailableNotificationTargetConfigurationTypes() {
      var availableNotificationTargetConfigurations = Assembly
        .GetExecutingAssembly()
        .GetTypes()
        .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(NotificationTargetConfigurationBase)))
        .ToList();
      return availableNotificationTargetConfigurations;
    }

    public static Type GetNotificationTargetConfigurationTypeByName(this IEnumerable<Type> availableNotificationTargetConfigurationTypes, string providerName) {
      return availableNotificationTargetConfigurationTypes.SingleOrDefault(t => IsMatchingNotificationTargetConfigurationType(providerName, t));
    }
    
    private static bool IsMatchingNotificationTargetConfigurationType(string providerName, MemberInfo providerType) {
      if (providerName == null)
        return false;
      
      var configTypeName = providerType.Name
        .Replace(nameof(NotificationTargetConfigurationBase), "")
        .Replace(nameof(NotificationTargetConfigurationBase).Replace("Base", ""), "");
      return string.Equals(providerName, configTypeName, StringComparison.InvariantCultureIgnoreCase)
             || string.Equals(providerName, providerType.Name, StringComparison.InvariantCultureIgnoreCase);
    }
  }
}
