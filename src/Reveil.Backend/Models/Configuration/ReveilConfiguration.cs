// ReSharper disable CollectionNeverUpdated.Global

using System;
using System.Collections.Generic;

namespace Reveil.Backend.Models.Configuration {
  public class ReveilConfiguration {
    public List<string> CrossOriginRequestOrigins { get; set; } = new();
    public string FullyQualifiedDomainName { get; set; } = Environment.MachineName;
    public bool RedirectToHttps { get; set; } = true;

    public I18NConfiguration I18N { get; set; } = new();
    public FileUploadConfiguration FileUploads { get; set; } = new();
    public PersistenceConfiguration Persistence { get; set; } = new();
    public AuthenticationConfiguration Authentication { get; set; } = new();
  }
}
