namespace Reveil.Backend.Structures {
  public enum JournalEntryType {
    CaseClosedByCreator = 101,
    NewMessageByCreator = 102,
    CaseClosedByManagement = 201,
    NewMessageByManagement = 202,
    CaseOpenedByCreator = 301,
    CaseDismissedByCreator = 302,
    CaseRetired = 303,
    UnauthenticatedRequestOnCase = 401
  }
}
