using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;

namespace Reveil.Backend.Middleware.Filter {
  public class DevelopmentEnvironmentAttribute : TypeFilterAttribute {
    public DevelopmentEnvironmentAttribute()
      : base(typeof(DevelopmentEnvironmentFilter)) {
    }
  }

  public class DevelopmentEnvironmentFilter : IActionFilter {
    private readonly IWebHostEnvironment _webHostEnvironment;

    public DevelopmentEnvironmentFilter(IWebHostEnvironment webHostEnvironment) {
      _webHostEnvironment = webHostEnvironment;
    }

    public void OnActionExecuting(ActionExecutingContext context) {
      if (_webHostEnvironment.IsDevelopment())
        return;

      throw new UnauthorizedAccessException($"{context.ActionDescriptor.DisplayName} is only allowed in a Development.");
    }

    public void OnActionExecuted(ActionExecutedContext context) {
    }
  }
}
