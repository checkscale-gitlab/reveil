// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.Net;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Models.Api.Responses {
  public class FileContentResponse : SuccessResponse {
    public int DataProcessingMethodHint { get; set; }
    public string FileId { get; set; }
    public string Data { get; set; }
    public int DataSize => Data.Length;

    public static FileContentResponse Create(FileEntity e, HttpStatusCode statusCode, string message = null) {
      var r = Create(statusCode, message);
      return new FileContentResponse {
        FileId = e.Id,
        Data = e.Data,
        DataProcessingMethodHint = e.DataProcessingMethodHint,
        Message = r.Message,
        DateTime = r.DateTime,
        IsSuccess = r.IsSuccess,
        StatusCode = r.StatusCode
      };
    }
  }
}
