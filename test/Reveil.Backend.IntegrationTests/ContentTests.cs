using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Reveil.Backend.Models.Api.Requests;
using Reveil.Backend.Models.Api.Responses.Core;
using Xunit;

namespace Reveil.Backend.IntegrationTests {
  public class ContentTests : IClassFixture<ReveilTestWebApplicationFactory<Startup>> {
    private readonly HttpClient _httpClient;

    public ContentTests(ReveilTestWebApplicationFactory<Startup> factory){
      _httpClient = factory.CreateClient(new WebApplicationFactoryClientOptions {
        AllowAutoRedirect = false
      });
    }

    [Fact]
    public async Task TestAddAndGetContent() {
      const string LanguageCode = "AT";
      const string ContentType = "Text";
      var getResult = await _httpClient.GetAsync($"/content/{ContentType}/{LanguageCode}");
      getResult.StatusCode.Should().Be(HttpStatusCode.NotFound);
      
      var req = new AddOrUpdateContentRequest {Content = "Hello", LanguageCode = LanguageCode, ContentType = ContentType};
      var body = JsonContent.Create(req);
      body.Headers.Add("Bearer", WellKnownConstants.IntegrationTestAuthorizationToken);
      var postResult = await _httpClient.PostAsync("/content", body);
      postResult.StatusCode.Should().Be(HttpStatusCode.Created);
      getResult = await _httpClient.GetAsync($"/content/{ContentType}/{LanguageCode}");
      getResult.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    
    [Fact]
    public async Task TestUpdateAndGetContent() {
      const string LanguageCode = "RU";
      const string ContentType = "Blyat";
      var getResult = await _httpClient.GetAsync($"/content/{ContentType}/{LanguageCode}");
      getResult.StatusCode.Should().Be(HttpStatusCode.NotFound);
      
      var req = new AddOrUpdateContentRequest {Content = "cyka", LanguageCode = LanguageCode, ContentType = ContentType};
      var body = JsonContent.Create(req);
      body.Headers.Add("Bearer", WellKnownConstants.IntegrationTestAuthorizationToken);
      var postResult = await _httpClient.PostAsync("/content", body);
      postResult.StatusCode.Should().Be(HttpStatusCode.Created);
      
      req = new AddOrUpdateContentRequest {Content = "Nano", LanguageCode = LanguageCode, ContentType = ContentType};
      body = JsonContent.Create(req);
      body.Headers.Add("Bearer", WellKnownConstants.IntegrationTestAuthorizationToken);
      postResult = await _httpClient.PostAsync("/content", body);
      postResult.StatusCode.Should().Be(HttpStatusCode.Created);

      var responseBodyAsString = await postResult.Content.ReadAsStringAsync();
      var response = JsonConvert.DeserializeObject<SuccessResponse>(responseBodyAsString);
      response.IsSuccess.Should().BeTrue();
    }
  }
}
