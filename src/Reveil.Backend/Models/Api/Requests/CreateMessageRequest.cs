// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Reveil.Backend.Models.Api.Requests {
  public class CreateMessageRequest {
    public int DataProcessingMethodHint { get; set; }
    public string PublicKey { get; set; }
    public string Data { get; set; }
  }
}
