import * as api from '~/api'

import { IModelJournal } from './models'

import { ApiEndpointExtensionLayer, ApiEndpointExtensionExtend } from '~/tsmodules/api/_eel'
import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '~/tsmodules/api/cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '~/tsmodules/api/crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '~/tsmodules/api/bootstrap'


export class ApiJournal extends ApiEndpointExtensionExtend {
    constructor(eel: ApiEndpointExtensionLayer) {
        super(eel)
    }

    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    // >!< journal
    // >!< >!< >!< >!< >!< >!< >!< >!< >!< >!< 
    async JournalExists(case_id: string, journal_id: string): Promise<boolean> {
        return new api.JournalApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getJournalEntry(case_id, journal_id)
        .then(() => {
            return true;
        })
        .catch((e) => {
            return false;
        })
    }

    async JournalGet(case_id: string, journal_id: string): Promise<IModelJournal> {
        // cache :: get
        var cache = ApiCache.Proxy<Array<IModelJournal>>(this.eel!.CacheType, "journal", this.eel!)
        var cache_find = cache.GetCache()?.find((e) => e.response.caseId === case_id && e.response.id === journal_id) || null
        if(cache_find !== null) {
            return cache_find
        }

        return new api.JournalApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getJournalEntry(case_id, journal_id)
        .then(async (e) => {
            var rtvl = {} as IModelJournal
            rtvl.response = e.data
            rtvl.modeltype = 'journal'

            // cache :: push
            cache.GetCache()?.push(rtvl)

            return rtvl;
        })
        .catch((e) => {
            throw Error('ModCase => journal_exists')
        })
    }

    async JournalGetAll(case_id: string): Promise<Array<IModelJournal>> {
        return new api.JournalApi(await ApiBootstrap.Proxy(this.eel!.BootstrapType, this.eel!).GetApiCfg()).getJournalEntriesForCase(case_id)
        .then((crl) => {
            var rtvl = []
            for(var journal_id of crl.data.ids || []) {
                rtvl.push(this.JournalGet(case_id, journal_id).then((e) => {
                    return e
                }))
            }
            return Promise.all(rtvl)
        })
        .then((e) => {
            e.sort((x, y) => ((x.response.creationDate || '') > (y.response.creationDate || '')) ? -1 : 1)
            return e
        })
        .catch((e) => {
            throw Error('ModCase => journal_getall')
        })
    }
}
