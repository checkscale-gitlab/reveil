import { ApiBootstrapModsIMod } from './mods/imod'
import { ApiBootstrapModsNuxt } from './mods/nuxt'
import { ApiBootstrapModsMock } from './mods/mock'

import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export enum ApiBootstrapType {
    Nuxt = 1,
    Mock,
}

export class ApiBootstrap {
    static Proxy(bootstrap_type: ApiBootstrapType, inject: ApiEndpointExtensionLayer): ApiBootstrapModsIMod {
        if(bootstrap_type === ApiBootstrapType.Nuxt) {
            // desc: nuxt-store cache
            return new ApiBootstrapModsNuxt(inject);
        }
        if(bootstrap_type === ApiBootstrapType.Mock) {
            // desc: mock-store cache
            return new ApiBootstrapModsMock(inject);
        }

        throw Error("Error. Module not found.")
    }
}
