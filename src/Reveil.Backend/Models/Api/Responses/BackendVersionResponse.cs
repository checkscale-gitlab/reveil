// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Reveil.Backend.Models.Api.Responses {
  public class BackendVersionResponse {
    public string Version { get; set; }
    public bool IsDevelopmentBuild { get; set; }
    public string SourceCodeCommitSha { get; set; }
  }
}
