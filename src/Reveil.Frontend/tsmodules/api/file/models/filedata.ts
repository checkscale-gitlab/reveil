import * as api from '~/api'
import { IModelFileDataPayload } from './filedatapayload';

export interface IModelFileData {
    response: api.FileMetadataResponse
    payload: IModelFileDataPayload,
}
