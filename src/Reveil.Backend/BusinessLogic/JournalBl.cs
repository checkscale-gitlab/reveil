using System.Linq;
using System.Net;
using CSharpFunctionalExtensions;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.BusinessLogic {
  public class JournalBl : IJournalBl {
    private readonly IGenericRepository<CaseEntity> _caseRepository;
    private readonly IGenericRepository<JournalEntity> _journalRepository;

    public JournalBl(IGenericRepository<CaseEntity> caseRepository, IGenericRepository<JournalEntity> journalRepository) {
      _caseRepository = caseRepository;
      _journalRepository = journalRepository;
    }

    private Result<CaseEntity, ErrorResponse> RequireExistingCase(string caseId) =>
      _caseRepository.Get(caseId).OrServerError().MapValueOrNotFound();


    public Result<JournalEntryResponse, ErrorResponse> GetJournalEntry(string caseId, string journalId) =>
      RequireExistingCase(caseId).Bind(_ => _journalRepository.Get(journalId).OrServerError().MapValueOrNotFound())
        .Map(j => JournalEntryResponse.Create(j, HttpStatusCode.OK));

    public Result<ChildrenListResponse, ErrorResponse> GetJournalEntriesByCase(string caseId) =>
      RequireExistingCase(caseId)
        .Bind(_ => _journalRepository.GetAll(m => m.CaseId == caseId).OrServerError())
        .Map(m => m.Select(x => x.Id).ToList())
        .Map(m => ChildrenListResponse.Create(m, HttpStatusCode.OK));
  }
}
