using System.IO;
using Microsoft.Extensions.Options;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Repositories {
  public class MessageEntityFileSystemRepository : AbstractGenericFileSystemPersistedRepository<MessageEntity> {
    private readonly IOptions<PersistenceConfiguration> _persistenceConfiguration;

    public MessageEntityFileSystemRepository(IOptions<PersistenceConfiguration> persistenceConfiguration) : base(persistenceConfiguration) {
      _persistenceConfiguration = persistenceConfiguration;
    }

    protected override string GetDataPath(MessageEntity baseEntity) {
      var basePath = Path.Join(
        _persistenceConfiguration.Value.DataRoot,
        baseEntity.GetType().Name.ToLower(),
        baseEntity.CaseId);
      Directory.CreateDirectory(basePath);
      return Path.Join(basePath, $"{baseEntity.Id}.json");
    }
  }
}
