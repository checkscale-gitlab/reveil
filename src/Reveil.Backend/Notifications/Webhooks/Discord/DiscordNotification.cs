using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Reveil.Backend.Notifications.Webhooks.Discord {
  public class DiscordNotification {
    [JsonPropertyName("content")] public string Content { get; set; }
    [JsonPropertyName("username")] public string Username { get; set; }
    [JsonPropertyName("avatar_url")] public string AvatarUrl { get; set; }
    [JsonPropertyName("embeds")] public object[] Embeds { get; set; }
    [JsonPropertyName("components")] public object[] Components { get; set; }
    [JsonPropertyName("allowed_mentions")] public DiscordAllowedMentions Mentions { get; set; }
  }

  public class DiscordAllowedMentions {
    [JsonPropertyName("parse")] public List<string> Parse { get; set; } = new();
    [JsonPropertyName("users")] public List<string> Users { get; set; } = new();
  }
  
  public class DiscordEmbedFooter {
    [JsonPropertyName("text")] public string Text { get; set; }
    [JsonPropertyName("icon_url")] public string IconUrl { get; set; }
  }

  public class DiscordEmbedAuthor {
    [JsonPropertyName("name")] public string Name { get; set; }
    [JsonPropertyName("url")] public string Url { get; set; }
    [JsonPropertyName("icon_url")] public string IconUrl { get; set; }
  }
  
  public class DiscordLinkEmbed {
    [JsonPropertyName("type")] public string Type => "link";
    [JsonPropertyName("url")] public string Url { get; set; }
    [JsonPropertyName("description")] public string Description { get; set; }
    [JsonPropertyName("color")] public int? Color { get; set; }
    [JsonPropertyName("title")] public string Title { get; set; }

    [JsonPropertyName("author")] public DiscordEmbedAuthor Author { get; set; } = null;
    [JsonPropertyName("footer")] public DiscordEmbedFooter Footer { get; set; } = null;
  }
  
  public class DiscordActionRowComponent {
    [JsonPropertyName("type")] public int Type => 1;
    [JsonPropertyName("components")] public List<object> Components { get; set; }
  }

  public class DiscordButtonComponent {
    [JsonPropertyName("type")] public int Type => 2;
    [JsonPropertyName("style")] public int Style => 3;
    [JsonPropertyName("url")] public string Url { get; set; }
    [JsonPropertyName("label")] public string Label { get; set; }
  }
}
