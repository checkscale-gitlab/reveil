using System.Net;
using Microsoft.AspNetCore.Mvc;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Middleware.Filter;
using Reveil.Backend.Models.Api.Requests;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Controllers {
  [ApiController]
  [Route("[controller]")]
  [Produces("application/json")]
  [Consumes("application/json")]
  public class MessagesController : ReveilBaseController {
    private readonly IMessagesBl _businessLogic;
    private readonly IGenericRepository<AuthorizationEntity> _authorizationRepository;

    public MessagesController(IMessagesBl businessLogic, IGenericRepository<AuthorizationEntity> authorizationRepository) {
      _businessLogic = businessLogic;
      _authorizationRepository = authorizationRepository;
    }

    /// <summary>
    /// Get list of messages for a case
    /// </summary>
    /// <param name="caseId">the Id of the case</param>
    /// <returns></returns>
    [HttpGet("/cases/{caseId}/messages", Name = nameof(GetMessagesInCase))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(ChildrenListResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetMessagesInCase(string caseId) => _businessLogic.GetMessagesByCase(caseId).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Gets the content of a message
    /// </summary>
    /// <param name="caseId">the case the message belongs to</param>
    /// <param name="messageId">the Id of the message</param>
    /// <returns></returns>
    [HttpGet(Routes.GetMessageRoute, Name = nameof(GetMessage))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetMessage(string caseId, string messageId) => _businessLogic.GetMessage(caseId, messageId).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Create a new message in a case
    /// </summary>
    /// <param name="caseId">the case the message belongs to</param>
    /// <param name="messageId">the Id of the message</param>
    /// <param name="request">the Public Key of the Creator and the (encrypted) message content</param>
    /// <returns></returns>
    [HttpPost("/cases/{caseId}/messages/{messageId}", Name = nameof(CreateMessage))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(SuccessResponse), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Conflict)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse CreateMessage(string caseId, string messageId, [FromBody] CreateMessageRequest request) =>
      _businessLogic
        .CreateMessage(caseId, messageId, request, GetUserId(GetAuthorizationToken()))
        .MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Get list of File Uploads for a message
    /// </summary>
    /// <param name="caseId">the case the message belongs to</param>
    /// <param name="messageId">the Id of the message</param>
    /// <returns></returns>
    [HttpGet("/cases/{caseId}/messages/{messageId}/files", Name = nameof(GetFiles))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(ChildrenListResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetFiles(string caseId, string messageId) =>
      _businessLogic.GetFiles(caseId, messageId).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Get Metadata for an already uploaded File
    /// </summary>
    /// <param name="caseId">the case the message of the FileUpload belongs to</param>
    /// <param name="messageId">the message the FileUpload belongs to</param>
    /// <param name="fileId">the Id of File message</param>
    /// <returns></returns>
    [HttpGet("/cases/{caseId}/messages/{messageId}/files/{fileId}/meta", Name = nameof(GetFileMetadata))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(FileMetadataResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetFileMetadata(string caseId, string messageId, string fileId) =>
      _businessLogic.GetFileMetadata(caseId, messageId, fileId).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Get an already uploaded File
    /// </summary>
    /// <param name="caseId">the case the message of the FileUpload belongs to</param>
    /// <param name="messageId">the message the FileUpload belongs to</param>
    /// <param name="fileId">the Id of File message</param>
    /// <returns></returns>
    [HttpGet("/cases/{caseId}/messages/{messageId}/files/{fileId}/data", Name = nameof(GetFileContent))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(FileContentResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse GetFileContent(string caseId, string messageId, string fileId) =>
      _businessLogic.GetFileContent(caseId, messageId, fileId).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Upload a new File
    /// </summary>
    /// <param name="caseId">the case the message of the FileUpload belongs to</param>
    /// <param name="messageId">the message the FileUpload belongs to</param>
    /// <param name="fileId">the Id of File message</param>
    /// <param name="fileUploadRequest"></param>
    /// <returns></returns>
    [HttpPost("/cases/{caseId}/messages/{messageId}/files/{fileId}", Name = nameof(UploadFile))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(SuccessResponse), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Conflict)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse UploadFile(string caseId, string messageId, string fileId, [FromBody] FileUploadRequest fileUploadRequest) =>
      _businessLogic.UploadFile(caseId, messageId, fileId, fileUploadRequest).MatchAndSetStatusCode(HttpContext);

    /// <summary>
    /// Remove an already uploaded File (permitted for initial uploader only)
    /// </summary>
    /// <param name="caseId">the case the message of the FileUpload belongs to</param>
    /// <param name="messageId">the message the FileUpload belongs to</param>
    /// <param name="fileId">the Id of File message</param>
    /// <returns></returns>
    [HttpDelete("/cases/{caseId}/messages/{messageId}/files/{fileId}", Name = nameof(DeleteFile))]
    [RequireAuthorization]
    [ProducesResponseType(typeof(SuccessResponse), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Conflict)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public BaseResponse DeleteFile(string caseId, string messageId, string fileId) =>
      _businessLogic.DeleteFile(caseId, messageId, fileId).MatchAndSetStatusCode(HttpContext);

    private string GetUserId(string authorizationToken) {
      var result = _authorizationRepository.Get(e => e.AuthorizationToken == authorizationToken);
      if (result.IsFailure || result.Value.HasNoValue)
        return null;
      return result.Value.Value.UserId;
    }
  }
}
