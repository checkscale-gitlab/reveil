import { Context as NuxtCtx } from '@nuxt/types'

import { ApiCache, ApiCacheModsIMod, ApiCacheType} from '../cache'
import { ApiCrypt, ApiCryptModsIMod, ApiCryptType } from '../crypt'
import { ApiBootstrap, ApiBootstrapModsIMod, ApiBootstrapType } from '../bootstrap'

export class ApiEndpointExtensionLayer {
    public BootstrapType: ApiBootstrapType
    public CacheType: ApiCacheType
    public CryptType: ApiCryptType
    public Context?: NuxtCtx

    constructor(
        bootstrap_type: ApiBootstrapType | null,
        cache_type: ApiCacheType | null,
        crypt_type: ApiCryptType | null,
        context?: NuxtCtx
    )
    {
        this.BootstrapType = bootstrap_type || ApiBootstrapType.Nuxt
        this.CacheType = cache_type || ApiCacheType.Nuxt
        this.CryptType = crypt_type || ApiCryptType.Plain
        this.Context = context
    }
}

export class ApiEndpointExtensionExtend {
    protected eel?: ApiEndpointExtensionLayer

    constructor(eel?: ApiEndpointExtensionLayer) {
        this.eel = eel
    }
}
