# reveil

<img src="/docs/assets/puzzle.png" alt="Reveil" width="128"/>
![Pipeline](https://gitlab.com/reveil/reveil/badges/main/pipeline.svg?ignore_skipped=true)
![Release](https://gitlab.com/reveil/reveil/-/badges/release.svg)

Reveil is a F(L)OSS whistleblower platform compliant with the general whistleblower regulation governed by the european union. It's written in ASP.NET (.NET 6) for the Backend and Vue (Nuxt) for the Frontend. It's open and interconnectable with your existing infrastructure, and its primary focus is the anonymity of the whistleblower. It's free, self-hostable, and extensible and open to modification.

## Why Reveil?

Unlawful activities and abuse of law may occur in any organisation, whether private or public, big or small. They can take many forms, corruption, fraud, businesses’ malpractice or negligence. And if they are not addressed, it can result in serious harm to the public interest. People who work for an organisation or are in contact with it in their work-related activities are often the first to know about such occurrences and are, therefore, in a privileged position to inform those who can address the problem. Whistleblowers, i.e. persons who report (within the organisation concerned or to an outside authority) or disclose (to the public) information on a wrongdoing obtained in a work-related context, help preventing damage and detecting threat or harm to the public interest that may otherwise remain hidden.

However, at European and national level the protection of whistleblowers is uneven and fragmented. As a consequence whistleblowers are often discouraged from reporting their concerns for fear of retaliation. For these reasons, on 23 April 2018, the European Commission presented a package of initiatives including a Proposal for Directive on the protection of persons reporting on breaches of Union law and a Communication, establishing a comprehensive legal framework for whistleblower protection for safeguarding the public interest at European level, setting up easily accessible reporting channels, underlining the obligation to maintain confidentiality and the prohibition of retaliation against whistleblowers and establishing targeted measures of protection.

The Directive - (EU) 2019/1937 of the European Parliament and of the Council of 23 October 2019 on the protection of persons who report breaches of Union law - was adopted on 23 October 2019 and entered into force on 16 December 2019. Member States have until 17 December 2021 to transpose it into their national laws.

## Further reading

- [Development](docs/development.md)
- [Backend Documentation](docs/backend.md)
