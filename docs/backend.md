# Reveil Backend

## Configuration Concept

Reveil utilizes a layered configuration mechanism, where each layer overrides the configured settings of the previous layer (if present).
Some layers allow hot-reloading of configuration (meaning you can modify while Reveil is running, without restarting Reveil for changes to take effect).
This effectively allows the configuration to be edited without the need to restart the backend.

### Ordering of Configuration Layers

Settings on a layer with a lower priority can be overriden by a setting in a higher-priority configuration layer.
Therefor, the order of layers is an important concept to understand when working with the Reveil configuration.

Internally Reveil utilizes the following Configuration Builder to build the final configuration layer.

```csharp
      builder
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile("appsettings.Development.json", optional: true, reloadOnChange: true)
        .AddJsonFile("Data/appsettings.json", optional: true, reloadOnChange: true)
        .AddUserSecrets<Startup>(true)
        .AddEnvironmentVariables();
```
Or in other words,

- the file `appsettings.json` is the lowest layer (it contains most of the sensible defaults)
- settings present in the `appsettings.Development.json` override settings in the `appsettings.json` file
- the `Data/appsettings.json` is the file you'll be doing your persistent productive settings, it'll override settings of the previous layers
- the [UserSecrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-6.0&tabs=linux) layer is supposed to store sensitive Secrets (e.g. Connection Strings, Passwords)
- finally the highest priority layer is Environment Variables. Each and every previously configured setting can be overriden by Environment Variables

### When to use which Configuration Layer

- in a development environment, use the `appsettings.Development.json` and the `dotnet user-secrets`
- in a production environment, either use `appsettings.json`, `Data/appsettings.json` or `Environment Variables`
- for production usage as container, either mount `Data/appsettings.json` as volume, or use `Environment Variables`

### Deriving Environment Variables

Json is a well-structured format that can contain nested objects. Environment-Variables are flat and "dumb" KeyValue-Pairs where the Key is a arbitrary string.
Overriding settings (even nested settings) from Environment Variables is still possible when following a simple set of rules and conventions outlined in this section.

#### Data Types

Oversimplified, `JSON` knows 3 data types. Environment Variables however do not differentiate between different Data Types.
Every value stored in a Environment Variable is just Text (`string`). Therefor mapping from non-typed Environment-Variables
to typed JSON is convention-based.

##### Text

JSON Variables holding Text are simple to export as Environment Variable as they match in type out-of-the-box. JSON additionally allows setting
values to `null` (`nil`, or in other words: `explicitly` not set). If Reveil requires a specific setting key that is null, it'll crash.

```json 
{
  "SomeSimpleText": "Hello World. How is it going?",
  "SomeEmptyText": "" ,
  "SomeUnsetText": null
}
```
The Environment Variables for the above JSON block would look like ...

- ... for Linux/UNIX-oid

  ```bash
  export SOMESIMPLETEXT='Hello World. How is it going?'
  export SOMEEMPTYTEXT=''
  # note: not exporting any Environment Variable for "SomeUnsetText"
  ```

- ... for Windows (with PowerShell)

  ```powershell
  $env:SOMESIMPLETEXT='Hello World. How is it going?'
  $env:SOMEEMPTYTEXT=''
  <# note: not setting any Environment Variable for "SomeUnsetText" #>
  ```

##### Numeric

Analogous to Text, a number in JSON can also be mapped to a Text Environment Variable fairly easy. However, there is a pitfall for floating point numbers, that you need to be aware.
Floating-Point numbers need to use `.` (dot character) as separator in Environment Variables. JSON additionally allows setting values to `null` (`nil`, or in other words: `explicitly` not set).
Reveil will default back to `0` if the Setting is not set at all or set to `null`.

```json
{
  "SomeInteger": 21,
  "SomeNegativeInteger": -42,
  "SomeFloatingPoint": 3.1415,
  "SomeNegativeFloatingPoint": -42.424242,
  "SomeUnsetNumeric": null
}
```

The Environment Variables for the above JSON block would look like ...

- ... for Linux/UNIX-oid

  ```bash
  export SOMEINTEGER='21'
  export SOMENEGATIVEINTEGER='-42'
  export SOMEFLOATINGPOINT='3.1415'
  export SOMENEGATIVEFLOATINGPOINT='-42.424242'
  # note: not exporting any Environment Variable for "SomeUnsetNumeric"
  ```

- ... for Windows (with PowerShell)

  ```powershell
  $env:SOMEINTEGER='21'
  $env:SOMENEGATIVEINTEGER='-42'
  $env:SOMEFLOATINGPOINT='3.1415'
  $env:SOMENEGATIVEFLOATINGPOINT='-42.424242'
  <# note: not setting any Environment Variable for "SomeUnsetNumeric" #>
  ```

##### Boolean

Typically a boolean can hold 2 States, either `True` or `False` (like `On` or `Off`). JSON additionally allows setting
values to `null` (`nil`, or in other words: `explicitly` not set). Reveil will default back to `False` if the Setting is not
set at all or set to `null`.

```json 
{
  "SomeTrueBoolean": true,
  "SomeFalseBoolean": false,
  "SomeUnsetBoolean": null
}
```
The Environment Variables for the above JSON block would look like ...

- ... for Linux/UNIX-oid

  ```bash
  export SOMETRUEBOOLEAN='true'
  export SOMEFALSEBOOLEAN='false'
  # note: not exporting any Environment Variable for "SomeUnsetBoolean"
  ```

- ... for Windows (with PowerShell)

  ```powershell
  $env:SOMETRUEBOOLEAN='true'
  $env:SOMEFALSEBOOLEAN='false'
  <# note: not setting any Environment Variable for "SomeUnsetBoolean" #>
  ```
#### Deriving the Key

##### Flat Keys

some settings in the Configuration of Reveil are organized "flat". Such keys map 1-to-1 to the Environment Variable.

```json
{
  "RedirectToHttps": true
}
```

To override this with a Environment Variable, the following Variable needs to be set 

```bash
REDIRECTTOHTTPS='true'
```


##### Nested Keys

most settings in the Reveil Configuration are nested (grouped by a parent key), to make the configuration better readable.
The convention to navigate into the nested object, is to use "__" (double underline).

```json
{
  "Persistence": {
    "DataRoot": "/home/rare/Downloads/reveil/data"
  }
}
```

To override this with a Environment Variable, the following Variable needs to be set

```bash
# Parent Key = "PERSISTENCE"; Key inside the Parent = "DATAROOT"
PERSISTENCE__DATAROOT='/home/rare/Downloads/reveil/data'
```

##### Arrays / Lists

Reveil also allows configuration of Lists / Arrays. An example would be the Allowed File Extensions that a Whistleblower can upload.
Those need to be allow-listed in the backend. The Array/List contents are considered a children of the parent key, so the same rules
apply as for nested object. However since arrays are ordered, the convention utilizes "Indices" to specify the exact position in a list.
Note that the index is beginning at `0` (not at `1` - looking at you Matlab).

```json
{
  "FileUploads": {
    "AllowedFileExtensions": [
      "png",
      "jpg"
    ]
  }
}
```

To override these with a Environment Variable, the following Variable(s) need(s) to be set

```bash
# Outermost Key = "FILEUPLOADS"; Parent Key = "ALLOWEDFILEEXTENSIONS"; Index/Position Key = 0 
FILEUPLOADS__ALLOWEDFILEEXTENSIONS__0='png'
# Outermost Key = "FILEUPLOADS"; Parent Key = "ALLOWEDFILEEXTENSIONS"; Index/Position Key = 1
FILEUPLOADS__ALLOWEDFILEEXTENSIONS__1='jpg'
```

## Backend Configuration

### Logging Configuration

|               AppSettings Key               |             Environment-Variable              |                   Description                   |
|:-------------------------------------------:|:---------------------------------------------:|:-----------------------------------------------:|
|          Logging:LogLevel:Default           |          Logging__LogLevel__Default           |     the Default Log level for log messages      |
|         Logging:LogLevel:Microsoft          |         Logging__LogLevel__Microsoft          |    Log Level for used Microsoft Dependencies    |
| Logging:LogLevel:Microsoft.Hosting.Lifetime | Logging__LogLevel__Microsoft.Hosting.Lifetime |          Log Level for hosted Services          |

### Web Server Configuration

|       AppSettings Key       |      Environment-Variable      |                   Description                   |
|:---------------------------:|:------------------------------:|:-----------------------------------------------:|
| Kestrel:EndPoints:Http:Url  | Kestrel__EndPoints__Http__Url  | HTTP Listen Address e.g. `http://localhost:80`  |
| Kestrel:EndPoints:Https:Url | Kestrel__EndPoints__Https__Url | HTTPS Listen Address e.g. `https://0.0.0.0:443` |
|        AllowedHosts         |          AllowedHosts          | Allowed request sources. `*` to allow allow all |
|       RedirectToHttps       |        RedirectToHttps         |      Whether HTTP shall redirect to HTTPS       |
|  FullyQualifiedDomainName   |    FullyQualifiedDomainName    |   FQDN of Reveil e.g. `reveil.yourdomain.tld`   |

- `AllowedHosts` defaults to `*`
- `RedirectToHttps` default to `true`
- `FullyQualifiedDomainName` defaults to `localhost`

### Cross-Origin Request Sources

CORS is disabled by default. It's not recommended to enable CORS in production. If CORS is configured, the Frontend shipped to a Whistleblower
would be capable of posting messages to a third-party Reveil-instance. While this sounds like a feature, it actually flaws the security model of 
Reveil.

### I18N and Localization

|     AppSettings Key      |   Environment-Variable    |                   Description                   |
|:------------------------:|:-------------------------:|:-----------------------------------------------:|
| I18N:DefaultLanguageCode | I18N__DefaultLanguageCode |     Two-Letter ISO Language Code e.g. `DE`      |
| I18N:DefaultCountryCode  | I18N__DefaultCountryCode  |      Two-Letter ISO Country Code e.g. `at`      |
|   I18N:DefaultTimezone   |   I18N__DefaultTimezone   | Olson Timezone Identifier e.g. `Europe/Vienna`  |

- `DefaultLanguageCode` defaults to null, the Reveil Frontend will try to derive the language from the User-Agent of the Browser
- `DefaultCountryCode` defaults to null, the Reveil Frontend will try to derive the Country from the User-Agent of the Browser
- `DefaultTimzeone` defaults to `UTC`


### Authentication

Reveil supports multiple ways of authentication for Administrators. You can either use a single method of authenticating an administrator, or combine multiple methods as a "chain of responsibility".
Each method of authentication is considered a "Provider of authentication". You can configure one or multiple providers to be present at the same time. The first provider successfully authenticating a user
is used to authenticate the user. If no provider in the "chain of responsibility" can authenticate the user, the authentication will fail.

#### Supported Providers

- Static
- Ldap

#### Static Authentication Provider

This provider allows adding administrators allowed to administrate Whistleblower cases from within the `appsettings` of Reveil.
This method of authenticating is not recommended for production instances of Reveil. Due to the password of a user being stored in plain-text,
this method is only recommended for "Testing" instances and "local playgrounds"!

##### JSON

```json
{
  "Authentication": {
    "Providers": [
      {
        "Type": "Static",
        "Administrators": [
          { 
            "Username":  "patrick@rdev.at", 
            "Password": "123", 
            "FirstName": "Patrick", 
            "LastName": "Rehberger", 
            "PhoneNumber": "123", 
            "UserMail": "patrick@rdev.at", 
            "DisplayName": "Patrick Rehberger"
          },
          {
            "Username":  "raffael@rtrace.io",
            "Password": "123",
            "FirstName": "Raffael",
            "LastName": "Rehberger",
            "PhoneNumber": "123",
            "UserMail": "raffael@rtrace.io",
            "DisplayName": "Patrick Rehberger"
          }
        ]
      }
    ]
  }
}
```

##### Environment Variables

```
AUTHENTICATION__PROVIDERS__0__TYPE = 'Static'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__0__USERNAME = 'patrick@rdev.at'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__0__PASSWORD = '123'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__0__FIRSTNAME = 'Patrick'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__0__LASTNAME = 'Rehberger'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__0__PHONENUMBER = '123'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__0__USERMAIL = 'patrick@rdev.at'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__0__DISPLAYNAME = 'Patrick Rehberger'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__1__USERNAME = 'raffael@rtrace.io'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__1__PASSWORD = '123'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__1__FIRSTNAME = 'Raffael'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__1__LASTNAME = 'Rehberger'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__1__PHONENUMBER = '123'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__1__USERMAIL = 'raffael@rtrace.io'
AUTHENTICATION__PROVIDERS__0__ADMINISTRATORS__1__DISPLAYNAME = 'Raffael Rehberger'

```

##### Settings

|            Key             |                                  Description                                  |
|:--------------------------:|:-----------------------------------------------------------------------------:|
|            Type            |                        required, needs to be "Static"                         |
|       Administrators       |   optional, a list/array of administrators, defaults to an empty list/array   |
|  Administrators.Username   |              required, used as the username to log-in to Reveil               |
|  Administrators.Password   |              required, used as the password to log-in to Reveil               |
|  Administrators.FirstName  |     required, the first name of the administrator (displayed in Frontend)     |
|  Administrators.LastName   |   required, the last name of the administrator (displayed in the Frontend)    |
| Administrators.PhoneNumber |  required, the phone number of the administrator (displayed in the Frontend)  |
|  Administrators.UserMail   | required, the E-Mail Address of the administrator (displayed in the Frontend) |
| Administrators.DisplayName |   required, the Full name of the administrator (displayed in the Frontend)    |


###### Security Considerations

Please note, that the `Administrators.Password` (in combination with the `Administrators.Username`) is considered a Secret and should not be exposed.
It is NOT recommended using the `Static` Authentication Provider in a production environment.

#### Ldap Provider

The Ldap Provider allows integration of Reveil into an existing domain with ease. Reveil connects to your LDAP compatible server and authenticates Administrator users against it.
Reveil will delegate administrative logins to the Ldap Server. If the LDAP server verifies that the user exists, the credentials are valid, and that the user is member of the specified Organizational Units (OUs), the
authentication is considered successful.

##### Supported LDAP implementations

- lldap (tested, recommended)
- openLDAP (tested)
- Active Directory (tbd, untested)

##### Using lldap for testing

You can test the Ldap Adapter locally with the lightweight LDAP implementation [lldap](https://github.com/nitnelave/lldap) from [nitnelave](https://github.com/nitnelave).

```bash
# starting lldap as lightweight authentication provider
podman run --name lldap \
  -v ./lldap_data:/data:Z \
  -p 3890:3890 -p 17170:17170 \
  -e LLDAP_JWT_SECRET=123abc \
  -e LLDAP_LDAP_USER_PASS=1234abcd \
  -e LLDAP_LDAP_BASE_DN=dc=rtrace,dc=io \
  nitnelave/lldap:stable
```

##### JSON

```json 
{
  "Authentication": {
    "Providers": [
      {
        "Type": "Ldap",
        "LdapServer": "ldap.yourcompany.tld",
        "LdapServerPort": 3890,
        "AllowedOUs": ["people", "lldap_admin", "administrators"],
        "DomainComponents": [ "rtrace", "io" ],
        "LdapAdminUsername": "admin",
        "LdapAdminPassword": "1234abcd",
        "UserIdAttribute": "uid",
        "MailAttribute": "mail",
        "FirstNameAttribute":"givenName",
        "LastNameAttribute": "sn",
        "DisplayNameAttribute": "cn",
        "PhoneNumberAttribute": "phoneNumber"
      }
    ]
  }
}
```

##### Environment Variables

```
AUTHENTICATION__PROVIDERS__0__TYPE = 'Ldap'
AUTHENTICATION__PROVIDERS__0__LDAPSERVER = 'ldap.yourcompany.tld'
AUTHENTICATION__PROVIDERS__0__LDAPSERVERPORT = '3890'
AUTHENTICATION__PROVIDERS__0__ALLOWEDOUS__0 = 'people'
AUTHENTICATION__PROVIDERS__0__ALLOWEDOUS__1 = 'Whistleblower Platform Management'
AUTHENTICATION__PROVIDERS__0__DOMAINCOMPONENTS__0 = 'rtrace'
AUTHENTICATION__PROVIDERS__0__DOMAINCOMPONENTS__1 = 'io'
AUTHENTICATION__PROVIDERS__0__LDAPADMINUSERNAME = 'admin'
AUTHENTICATION__PROVIDERS__0__LDAPADMINPASSWORD = '1234abcd'
AUTHENTICATION__PROVIDERS__0__USERIDATTRIBUTE = 'uid'
AUTHENTICATION__PROVIDERS__0__MAILATTRIBUTE = 'mail'
AUTHENTICATION__PROVIDERS__0__FIRSTNAMEATTRIBUTE = 'givenName'
AUTHENTICATION__PROVIDERS__0__LASTNAMEATTRIBUTE = 'sn'
AUTHENTICATION__PROVIDERS__0__DISPLAYNAMEATTRIBUTE = 'cn'
AUTHENTICATION__PROVIDERS__0__PHONENUMBERATTRIBUTE = 'phoneNumber'
```

##### Settings

|         Key          |                                               Description                                                |
|:--------------------:|:--------------------------------------------------------------------------------------------------------:|
|         Type         |                                       required, needs to be "Ldap"                                       |
|      LdapServer      |                                  required, the FQDN of the LDAP Server                                   |
|    LdapServerPort    |                            required, the port the LDAP Server is listening on                            |
|      AllowedOUs      |   required, array/list of OUs, users out of these Organizational Units are qualified as Administrators   |
|   DomainComponents   |                      required, which DCs are used to identify the User tree in LDAP                      |
|  LdapAdminUsername   |                   required, the LDAP username allowing Reveil to query the LDAP server                   |
|  LdapAdminPassword   |                   required, the LDAP password allowing Reveil to query the LDAP server                   |
|   UserIdAttribute    |         optional, the name of the LDAP attribute storing the UserName/UserId, defaults to "uid"          |
|    MailAttribute     |    optional, the name of the LDAP attribute storing the E-Mail address of a user, defaults to "mail"     |
|  FirstNameAttribute  |    optional, the name of the LDAP attribute storing the FirstName of a user, defaults to "givenName "    |
|  LastNameAttribute   |        optional, the name of the LDAP attribute storing the LastName of a user, defaults to "sn"         |
| DisplayNameAttribute | optional, the name of the LDAP attribute storing a Display Name (CommonName) of a user, defaults to "cn" |
| PhoneNumberAttribute |  optional, the name of the LDAP attribute storing the phone number of a user, defaults to "phoneNumber"  |

Note, that the settings of all `*Attribute`-keys need to match the LDAP implementation. The Defaults/Fallback-settings used in Reveil are
compatible with `lldap`. If you are using a different implementation (e.g. Active Directory), you might need to adapt some of the Attributes.
If you found a working configuration, please feel free to create a Pull-Request including this information in the documentation.

###### Security Considerations

Please note, that the `LdapAdminPassword` (in combination with the `LdapAdminUsername`) is considered a Secret and should not be exposed.

### Data Persistence

Reveil, as of now, supports a File-System based persistence method. This method was desired for the MVP, as its convenient to manage and to backup.
It's also a common-starting point for supporting cloud-ready storages like Minio and S3 or Azure Blob Storage for persisting Data of Reveil.
The DataRoot-Variable configures the place where Reveil will save its content and its Data. This path needs to be configured to match the expected location.
Reveil utilizes Copy-on-Write. So it is safe to backup/snapshot the Contents of the Directory while Reveil is running. Also Data can be imported into another 
Reveil instance by copying it. This is very helpful for creating Staging-Environments/Test-Environments with a production dataset.

##### JSON

```json
{
  "Persistence": {
    "DataRoot": "/mnt/disk/reveil/data"
  }
}
```

##### Environment Variables

```
PERSISTENCE__DATAROOT = '/mnt/disk/reveil/data'
```

Note, that the path MUST be a directory. If that directory does not exist (yet), Reveil will create it.

### Notifications

Reveil can notify case-administrators about certain events that require administrators' attention. This prevents administrators from checking (polling) for new cases and/or messages on frequent basis.
With notifications configured, administrators can rely on "push notifications" to well-known notification channels (like Mail). Those channels are supposed to be checked more frequently than Reveil itself.

#### Supported Notification Types

Reveil sends out Notifications on specific events. Events that trigger Notifications as of the latest version are:

- Case Created / Opened
- State Changed for Case (e.g. from Open to Closed)
- Message created for Case

#### Supported Notification Targets

a notification target is a platform/messenger/chat-system/mailserver a Notification can be sent to. Reveil can be configured to send Notifications to one or multiple targets.

- Discord
- Generic Webhook
- SMTP (E-Mail)

#### Discord Notification Target

##### JSON

```json
{
  "Notification": {
    "Targets": [
      {
        "Type": "Discord",
        "WebhookUrl": "https://discordapp.com/api/webhooks/smth/smth",
        "CustomMessage": "Checkout Reveil. There's something to do",
        "AllowMentionRoles": true,
        "AllowMentionEveryone": true,
        "AllowMentionEveryoneOnline": true,
        "MentionEveryone": true,
        "MentionEveryoneOnline": true,
        "CustomMentions": [
          "@franz",
          "@xXSp33dm4st3RXx"
        ],
        "AvatarUrl": "https://gitlab.com/reveil/reveil/-/raw/main/docs/assets/puzzle.png"
      }
    ]
  }
}
```

##### Environment Variables

```
NOTIFICATION__TARGETS__0__TYPE = "Discord"
NOTIFICATION__TARGETS__0__WEBHOOKURL = "https://discordapp.com/api/webhooks/smth/smth"
NOTIFICATION__TARGETS__0__ALLOWMENTIONROLES = "True"
NOTIFICATION__TARGETS__0__ALLOWMENTIONEVERYONE = "True"
NOTIFICATION__TARGETS__0__ALLOWMENTIONEVERYONEONLINE = "True"
NOTIFICATION__TARGETS__0__MENTIONEVERYONE = "True"
NOTIFICATION__TARGETS__0__MENTIONEVERYONEONLINE = "True"
NOTIFICATION__TARGETS__0__CUSTOMMENTIONS__0 = "@franz"
NOTIFICATION__TARGETS__0__CUSTOMMENTIONS__1 = "@xXSp33dm4st3RXx"
NOTIFICATION__TARGETS__0__AVATARURL = "https://gitlab.com/reveil/reveil/-/raw/main/docs/assets/puzzle.png"
```

##### Settings

|            Key             |                                                                                    Description                                                                                     |
|:--------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|            Type            |                                                                           required, needs to be Discord                                                                            |
|         WebHookUrl         | required, the Discord Webhook URL (can be generated from within the Discord Client, see [Discord Docs](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks)) |
|     AllowMentionRoles      |                                        optional, needs to be set to true, if the CustomMessage contains a specific role, defaults to false                                         |
|    AllowMentionEveryone    |                                           optional, needs to be set to true, if the CustomMessage contains @everyone, defaults to false                                            |
| AllowMentionEveryoneOnline |                                           optional, needs to be set to true, if the the CustomMessage contains @here, defaults to false                                            |
|      MentionEveryone       |                                              optional, if set to true, Reveil will automatically mention @everyone, defaults to false                                              |
|   MentionEveryoneOnline    |                                                optional, if set to true, Reveil will automatically mention @here, defaults to false                                                |
|       CustomMentions       |                                    optional, a list of Usernames or Roles that will be mentioned for each Notification, defaults to empty list                                     |
|         AvatarUrl          |                                             optional, HTTPS URL to a picture used as "User-image", defaults to the Reveil puzzle Logo                                              |

###### Security Considerations

Please note, that the `WebHookUrl` is considered a Secret and should not be exposed.
If the `WebHookUrl` is leaked, an attacker could potentially send fake notifications linking to fake Reveil Instances to "phish" for Administrator login credentials

#### SMTP Mail Notification Target

##### JSON

```json
{
  "Notification": {
    "Targets": [
      {
        "Type": "Smtp",
        "SenderName": "Reveil Mailer",
        "SenderEmail": "reveil-noreply@company.tld",
        "Recipients": [
          {
            "RecipientName": "Hans Maier",
            "RecipientEmail": "hans.maier@company.tld"
          },
          {
            "RecipientName": "Peter Huber",
            "RecipientEmail": "peter.huber@company.tld"
          }
        ],
        "SmtpServer": "mail.company.tld",
        "SmtpPort": 465,
        "UseSsl": true,
        "UseSmtpAuthentication": true,
        "SmtpServerUsername": "reveil-noreply@company.tld",
        "SmtpServerPassword": "mySuperS3cur3P4sSw0rd"
      }
    ]
  }
}
```

##### Environment Variables

```
NOTIFICATION__TARGETS__0__TYPE = "Smtp"
NOTIFICATION__TARGETS__0__SENDERNAME = "Reveil Mailer"
NOTIFICATION__TARGETS__0__SENDEREMAIL = "reveil-noreply@company.tld"
NOTIFICATION__TARGETS__0__RECIPIENTS__0__RECIPIENTNAME = "Hans Maier"
NOTIFICATION__TARGETS__0__RECIPIENTS__0__RECIPIENTEMAIL = "hans.maier@company.tld"
NOTIFICATION__TARGETS__0__RECIPIENTS__1__RECIPIENTNAME = "Peter Huber"
NOTIFICATION__TARGETS__0__RECIPIENTS__1__RECIPIENTEMAIL = "peter.huber@company.tld"
NOTIFICATION__TARGETS__0__SMTPSERVER = "mail.company.tld"
NOTIFICATION__TARGETS__0__SMTPPORT = "456"
NOTIFICATION__TARGETS__0__USESSL = "True"
NOTIFICATION__TARGETS__0__USESMTPAUTHENTICATION = "True"
NOTIFICATION__TARGETS__0__SMTPSERVERUSERNAME = "reveil-noreply@company.tld"
NOTIFICATION__TARGETS__0__SMTPSERVERPASSWORD = "mySuperS3cur3P4sSw0rd"
```

##### Settings

|            Key            |                                                       Description                                                       |
|:-------------------------:|:-----------------------------------------------------------------------------------------------------------------------:|
|           Type            |                                              required, needs to be "Smtp"                                               |
|        SenderName         |        optional, the name of the Sender that will be displayed in the E-Mail Client, defaults to "Reveil Mailer"        |
|        SenderEmail        |                                       required, the E-Mail address of the sender                                        |
|        Recipients         |                        optional, a list of recipient the mail is sent to, defaults to empty list                        |
| Recipients.RecipientName  |                 required, the Display Name of the User that will be used in the E-Mail Body as greeting                 |
| Recipients.RecipientEmail |                         required, the E-Mail address of the Recipient the mail will be sent to                          |
|        SmtpServer         |                       required, the FQDN of the Mailserver that shall be used to send the E-Mail                        |
|         SmtpPort          |                          required, the TCP-Port the Mailserver is accepting incoming requests                           |
|          UseSsl           |   required, whether Reveil should use Ssl when sending the E-Mail to the Mailserver (Mailserver needs to support SSL)   |
|   UseSmtpAuthentication   | optional, if set to false the Credentials below (Username and Password) are transferred in plain-text, defaults to true |
|    SmtpServerUsername     |                       required if "UseSmtpAuthentication" is true, Username of the E-Mail account                       |
|    SmtpServerPassword     |                       required if "UseSmtpAuthentication" is true, Password of the E-Mail account                       |

###### Security Considerations

Please note, that the `SmtpServerPassword` (in combination with the `SmtpServerUsername`) is considered a Secret and should not be exposed.
If the `SmtpServerPassword` is leaked, an attacker could potentially send fake notifications linking to fake Reveil Instances to "phish" for Administrator login credentials.

