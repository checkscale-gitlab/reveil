using System;
using System.Linq;
using System.Net;
using CSharpFunctionalExtensions;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Api.Responses.Primitives;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Structures;

namespace Reveil.Backend.BusinessLogic {
  public class CasesBl : ICasesBl {
    private readonly IGenericRepository<CaseEntity> _caseRepository;
    private readonly IGenericRepository<JournalEntity> _journalRepository;
    private readonly IGenericRepository<MessageEntity> _messageRepository;
    private readonly INotificationSenderFactory _notificationSenderFactory;

    public CasesBl(
      IGenericRepository<CaseEntity> caseRepository,
      IGenericRepository<JournalEntity> journalRepository,
      IGenericRepository<MessageEntity> messageRepository,
      INotificationSenderFactory notificationSenderFactory) {
      _caseRepository = caseRepository;
      _journalRepository = journalRepository;
      _messageRepository = messageRepository;
      _notificationSenderFactory = notificationSenderFactory;
    }

    private Result<Maybe<CaseEntity>, ErrorResponse> GetMaybeCase(string caseId) =>
      _caseRepository.Get(c => c.Id == caseId).OrServerError();

    private Result<CaseEntity, ErrorResponse> GetCaseInternal(string caseId) =>
      GetMaybeCase(caseId).MapValueOrNotFound();

    public Result<SuccessResponse, ErrorResponse> CaseExists(string caseId) =>
      GetCaseInternal(caseId)
        .Map(_ => SuccessResponse.Create(HttpStatusCode.Found, $"Case with Id {caseId} exists"));

    public Result<SuccessResponse, ErrorResponse> DeleteCase(string caseId) =>
      _caseRepository.Remove(caseId)
        .Map(() => SuccessResponse.Create(HttpStatusCode.OK, $"Deleted case with Id {caseId}"))
        .OrServerError();

    public Result<CaseResponse, ErrorResponse> GetCase(string caseId) {
      var messageIds = _messageRepository
        .GetAll(c => c.CaseId == caseId)
        .Map(l => l.Select(m => new AddressableId(m.Id, $"/cases/{caseId}/messages/{m.Id}", Routes.GetMessageRoute)).ToList())
        .OrServerError();

      var journalIds = _journalRepository
        .GetAll(c => c.CaseId == caseId)
        .Map(l => l.Select(m => new AddressableId(m.Id, $"/cases/{caseId}/journal/{m.Id}", Routes.GetJournalRoute)).ToList())
        .OrServerError();

      return GetCaseInternal(caseId)
        .Map(c => CaseResponse.Create(c, messageIds.Value, journalIds.Value, HttpStatusCode.OK, $"Case with Id {caseId} exists"));
    }

    public Result<SuccessResponse, ErrorResponse> OpenCase(string caseId, string authorizationToken) =>
      GetMaybeCase(caseId)
        .Ensure(c => c.HasNoValue, ErrorResponse.Create(HttpStatusCode.Conflict, $"Case with Id {caseId} does exist already"))
        .Bind(_ => _caseRepository.Add(new CaseEntity
            {Id = caseId, CreationDate = DateTime.UtcNow, State = CaseState.Open, AuthorizationToken = authorizationToken})
          .MapError(e => ErrorResponse.Create(HttpStatusCode.InternalServerError, e)))
        .Bind(_ => _journalRepository
          .Add(new JournalEntity
            {Id = Guid.NewGuid().ToString(), CaseId = caseId, CreationDate = DateTime.UtcNow, JournalType = JournalEntryType.CaseOpenedByCreator})
          .MapError(e => ErrorResponse.Create(HttpStatusCode.InternalServerError, e)))
        .Tap(_ => _notificationSenderFactory
          .Create()
          .SendAsync(new Notification {CaseId = caseId, NotificationType = NotificationType.NewCase, Message = $"Case with Id {caseId} opened"})
          .GetAwaiter()
          .GetResult())
        .Map(_ => SuccessResponse.Create(HttpStatusCode.Created, "Case opened"));

    public Result<SuccessResponse, ErrorResponse> CloseCase(string caseId) =>
      GetCaseInternal(caseId)
        .Bind(c => _journalRepository.Add(new JournalEntity
            {CaseId = c.Id, Id = Guid.NewGuid().ToString(), CreationDate = DateTime.UtcNow, JournalType = JournalEntryType.CaseClosedByCreator})
          .MapError(e => ErrorResponse.Create(HttpStatusCode.InternalServerError, e)))
        .Tap(_ => _notificationSenderFactory
          .Create()
          .SendAsync(new Notification {CaseId = caseId, NotificationType = NotificationType.NewCase, Message = $"Case with Id {caseId} closed"})
          .GetAwaiter()
          .GetResult())
        .Map(_ => SuccessResponse.Create(HttpStatusCode.OK, "Case closed"));

    public Result<SuccessResponse, ErrorResponse> UpdateCaseState(string caseId, CaseState state) =>
      GetCaseInternal(caseId)
        .Bind(c => _caseRepository.Remove(c.Id)
          .Bind(() => _caseRepository.Add(new CaseEntity
            {AuthorizationToken = c.AuthorizationToken, CreationDate = c.CreationDate, Id = caseId, State = state}))
          .OrServerError())
        .Tap(_ => _notificationSenderFactory
          .Create()
          .SendAsync(new Notification {CaseId = caseId, NotificationType = NotificationType.NewCase, Message = $"Case with Id {caseId} changed State to {state.ToString()}"})
          .GetAwaiter()
          .GetResult())
        .Map(_ => SuccessResponse.Create(HttpStatusCode.OK, "Case state changed"));

    public Result<ChildrenListResponse, ErrorResponse> GetAllCases() =>
      _caseRepository.GetAll(_ => true).OrServerError()
        .Map(m => m.Select(x => x.Id).ToList())
        .Map(m => ChildrenListResponse.Create(m, HttpStatusCode.OK));
  }
}
