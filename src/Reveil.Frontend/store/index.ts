import { Store } from 'vuex'
import { initialiseStores } from '~/utils/stores'

const initializer = (store: Store<any>) => initialiseStores(store)

export const plugins = [initializer]
// export * from '~/utils/stores'