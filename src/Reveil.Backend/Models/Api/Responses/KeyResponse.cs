// ReSharper disable UnusedAutoPropertyAccessor.Global

using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Models.Api.Responses {
  public class KeyResponse : SuccessResponse {
    public string Key { get; set; }
  }
}
