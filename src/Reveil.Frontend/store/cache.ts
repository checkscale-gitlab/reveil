import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'

@Module({
    name: 'cache',
    stateFactory: true,
    namespaced: true,
})

export default class StoreCache extends VuexModule {
    authuser = null
    get GetAuthuser(): any {
        return this.authuser
    }
    @Mutation
    SetAuthuser(data: any) {
        this.authuser = data
    }
    @Mutation
    ClearAuthuser(): void {
        this.authuser = null
    }

    authmgmt = null
    get GetAuthmgmt(): any {
        return this.authmgmt
    }
    @Mutation
    SetAuthmgmt(data: any) {
        this.authmgmt = data
    }
    @Mutation
    ClearAuthmgmt(): void {
        this.authmgmt = null
    }

    bootstrap = null
    get GetBootstrap(): any {
        return this.bootstrap
    }
    @Mutation
    SetBootstrap(data: any) {
        this.bootstrap = data
    }
    @Mutation
    ClearBootstrap(): void {
        this.bootstrap = null
    }

    message = null
    get GetMessage(): any {
        return this.message
    }
    @Mutation
    SetMessage(data: any) {
        this.message = data
    }
    @Mutation
    ClearMessage(): void {
        this.message = null
    }

    mgmt = null
    get GetMgmt(): any {
        return this.mgmt
    }
    @Mutation
    SetMgmt(data: any) {
        this.mgmt = data
    }
    @Mutation
    ClearMgmt(): void {
        this.mgmt = null
    }
}
