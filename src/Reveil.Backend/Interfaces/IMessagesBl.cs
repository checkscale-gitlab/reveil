using CSharpFunctionalExtensions;
using Reveil.Backend.Models.Api.Requests;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;

namespace Reveil.Backend.Interfaces {
  public interface IMessagesBl {
    Result<ChildrenListResponse, ErrorResponse> GetFiles(string caseId, string messageId);
    Result<SuccessResponse, ErrorResponse> DeleteFile(string caseId, string messageId, string fileId);
    Result<ChildrenListResponse, ErrorResponse> GetMessagesByCase(string caseId);
    Result<MessageResponse, ErrorResponse> GetMessage(string caseId, string messageId);
    Result<SuccessResponse, ErrorResponse> CreateMessage(string caseId, string messageId, CreateMessageRequest request, string authorId = null);
    Result<FileContentResponse, ErrorResponse> GetFileContent(string caseId, string messageId, string fileId);
    Result<FileMetadataResponse, ErrorResponse> GetFileMetadata(string caseId, string messageId, string fileId);
    Result<SuccessResponse, ErrorResponse> UploadFile(string caseId, string messageId, string fileId, FileUploadRequest r);
  }
}
