using FluentAssertions;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.ContentBl {
  public class AddOrUpdateContentTests {
    [Theory]
    [InlineData(nameof(AddOrUpdateContentTests), "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", nameof(AddOrUpdateContentTests), RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData(nameof(AddOrUpdateContentTests), "0123456789ABCDEF", RepoType.FilePersisted)]
    [InlineData("0123456789ABCDEF", nameof(AddOrUpdateContentTests), RepoType.FilePersisted)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestAddNewPreviouslyNotExistingContent(string contentType, string contentLanguage, RepoType repoType) {
      const string Text = "Hello";
      var repo = RepositoryFactory.Create<ContentEntity>(repoType);
      var bl = new Backend.BusinessLogic.ContentBl(repo);
      var r = bl.AddOrUpdateContent(contentType, contentLanguage, Text);
      r.IsSuccess.Should().BeTrue();
      r.Value.IsSuccess.Should().BeTrue();
      r.Value.Content.Should().Be(Text);
      r.Value.Language.Should().Be(contentLanguage);

      repo.Remove(_ => true);
    }

    [Theory]
    [InlineData(nameof(AddOrUpdateContentTests), "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", nameof(AddOrUpdateContentTests), RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData(nameof(AddOrUpdateContentTests), "0123456789ABCDEF", RepoType.FilePersisted)]
    [InlineData("0123456789ABCDEF", nameof(AddOrUpdateContentTests), RepoType.FilePersisted)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestOverwritePreviouslyExistingContent(string contentType, string contentLanguage, RepoType repoType) {
      const string Text = "Hello";
      var repo = RepositoryFactory.Create<ContentEntity>(repoType);
      var bl = new Backend.BusinessLogic.ContentBl(repo);
      bl.AddOrUpdateContent(contentType, contentLanguage, "World");
      var r = bl.AddOrUpdateContent(contentType, contentLanguage, Text);
      r.IsSuccess.Should().BeTrue();
      r.Value.IsSuccess.Should().BeTrue();
      r.Value.Content.Should().Be(Text);
      r.Value.Language.Should().Be(contentLanguage);
      repo.Remove(_ => true);
    }
  }
}
