// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Reveil.Backend.Models.Api.Responses.Primitives {
  public class MediaType {
    public MediaType(string extension, string name) {
      Extension = extension;
      Name = name;
    }

    public string Extension { get; set; }
    public string Name { get; set; }
  }
}
