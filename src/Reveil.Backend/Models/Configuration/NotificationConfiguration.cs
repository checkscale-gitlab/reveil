using System.Collections.Generic;
using Reveil.Backend.Models.Configuration.Notification;

namespace Reveil.Backend.Models.Configuration {
  public class NotificationConfiguration {
    public List<NotificationTargetConfigurationBase> Targets { get; set; } = new();
  }
}
