// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System;

namespace Reveil.Backend.Models.Configuration {
  public class I18NConfiguration {
    public string DefaultCountryCode { get; set; } = "en";
    public string DefaultLanguageCode { get; set; } = "en";
    public string DefaultTimezone { get; set; }

    public TimeZoneInfo Timezone =>
      string.IsNullOrEmpty(DefaultTimezone)
        ? TimeZoneInfo.Utc
        : TimeZoneInfo.FindSystemTimeZoneById(DefaultTimezone);
  }
}
