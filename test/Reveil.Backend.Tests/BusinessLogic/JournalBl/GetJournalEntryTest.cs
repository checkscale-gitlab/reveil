using System.Net;
using FluentAssertions;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Structures;
using Reveil.Backend.Tests.PersistenceProvider;
using Xunit;

namespace Reveil.Backend.Tests.BusinessLogic.JournalBl {
  public class GetJournalEntryTest {
    [Theory]
    [InlineData("0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestNotExistingCaseReturns404ErrorResponse(string caseId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var bl = new Backend.BusinessLogic.JournalBl(cases, journals);
      var r = bl.GetJournalEntry(caseId, caseId);
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestGetNotExistingJournalEntryForExistingCaseReturns404Response(string caseId, string messageId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var bl = new Backend.BusinessLogic.JournalBl(cases, journals);
      cases.Add(new CaseEntity {Id = caseId});
      var r = bl.GetJournalEntry(caseId, messageId);
      r.IsFailure.Should().BeTrue();
      r.Error.Should().BeOfType<ErrorResponse>();
      r.Error.StatusCode.Should().Be(HttpStatusCode.NotFound);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
    }

    [Theory]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.InMemory)]
    [InlineData("0123456789ABCDEF", "0123456789ABCDEF", RepoType.FilePersisted)]
    public void TestGetExistingJournalEntryForExistingCaseReturns200Response(string caseId, string journalId, RepoType repoType) {
      var cases = RepositoryFactory.Create<CaseEntity>(repoType);
      var journals = RepositoryFactory.Create<JournalEntity>(repoType);
      var bl = new Backend.BusinessLogic.JournalBl(cases, journals);
      cases.Add(new CaseEntity {Id = caseId});
      journals.Add(new JournalEntity {CaseId = caseId, Id = journalId, JournalType = JournalEntryType.CaseOpenedByCreator});
      var r = bl.GetJournalEntry(caseId, journalId);
      r.IsSuccess.Should().BeTrue();
      r.Value.Should().BeOfType<JournalEntryResponse>();
      r.Value.Code.Should().Be((int)JournalEntryType.CaseOpenedByCreator);
      r.Value.StatusCode.Should().Be(HttpStatusCode.OK);
      r.Value.Id.Should().Be(journalId);
      r.Value.CaseId.Should().Be(caseId);
      cases.Remove(_ => true);
      journals.Remove(_ => true);
    }
  }
}
