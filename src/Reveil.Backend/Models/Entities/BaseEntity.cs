namespace Reveil.Backend.Models.Entities {
  public class BaseEntity {
    public string Id { get; set; }
  }
}
