import { remove } from 'exifremove'

export function stripexif(content) {
    var buff = Buffer.from(content, 'utf-8')
    return buff.toString(remove(buff))
}
