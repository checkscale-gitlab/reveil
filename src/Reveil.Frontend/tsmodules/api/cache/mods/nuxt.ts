import { ApiCacheModsIMod } from './imod'
import { ApiEndpointExtensionLayer } from '~/tsmodules/api/_eel'

export class ApiCacheModsNuxt<T> implements ApiCacheModsIMod<T> {
    private name: string
    private inject: ApiEndpointExtensionLayer

    constructor(name: string, inject: ApiEndpointExtensionLayer) {
        this.name = name.charAt(0).toUpperCase() + name.slice(1)
        this.inject = inject
    }

    GetCache(): T | null {
        return this.inject.Context?.store.getters['cache/Get' + this.name] as T || null;
    }

    SetCache(data: T): void {
        this.inject.Context?.store.commit('cache/Set' + this.name, data)
    }

    ClearCache() {
        this.inject.Context?.store.commit('cache/Clear' + this.name)
    }

    WipeCache(): void {
        
    }
}
