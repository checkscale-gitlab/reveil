using System.IO;
using Microsoft.Extensions.Options;
using Reveil.Backend.Models.Configuration;
using Reveil.Backend.Models.Entities;

namespace Reveil.Backend.Repositories {
  public class ContentEntityFileSystemPersistedRepository : AbstractGenericFileSystemPersistedRepository<ContentEntity> {
    private readonly IOptions<PersistenceConfiguration> _persistenceConfiguration;

    public ContentEntityFileSystemPersistedRepository(IOptions<PersistenceConfiguration> persistenceConfiguration) : base(persistenceConfiguration) {
      _persistenceConfiguration = persistenceConfiguration;
    }

    protected override string GetDataPath(ContentEntity baseEntity) {
      var basePath = Path.Join(_persistenceConfiguration.Value.DataRoot, baseEntity.GetType().Name.ToLower(), baseEntity.Type);
      Directory.CreateDirectory(basePath);
      return Path.Join(basePath, $"{baseEntity.Type}-{baseEntity.Language}.json");
    }

    protected override string BuildIndexPath(string id) =>
      Path.Join(GetIndexBasePath(), $"{id}.idx");
  }
}
