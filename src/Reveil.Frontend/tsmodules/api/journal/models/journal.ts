import * as api from '~/api'

export interface IModelJournal {
    modeltype: string,
    
    response: api.JournalEntryResponse,
}
