#!/usr/bin/env sh

# workaround for webpack-related issue during build-time.
# https://github.com/webpack/webpack/issues/14532
export NODE_OPTIONS=--openssl-legacy-provider

# npm --prefix src/Reveil.Frontend install
# npm --prefix src/Reveil.Frontend audit fix || true
# npm --prefix src/Reveil.Frontend run test
