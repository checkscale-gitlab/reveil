// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.Collections.Generic;

namespace Reveil.Backend.Models.Configuration {
  public class FileUploadConfiguration {
    public List<string> AllowedFileExtensions { get; set; } = new();
    public int MaxFileUploadSizeInBytes { get; set; } = 10 * 1024 * 1024; // 10 MB
  }
}
