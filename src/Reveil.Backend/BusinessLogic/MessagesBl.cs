using System;
using System.Linq;
using System.Net;
using CSharpFunctionalExtensions;
using Reveil.Backend.Extensions;
using Reveil.Backend.Interfaces;
using Reveil.Backend.Models.Api.Requests;
using Reveil.Backend.Models.Api.Responses;
using Reveil.Backend.Models.Api.Responses.Core;
using Reveil.Backend.Models.Entities;
using Reveil.Backend.Notifications;
using Reveil.Backend.Structures;

namespace Reveil.Backend.BusinessLogic {
  public class MessagesBl : IMessagesBl {
    private readonly IGenericRepository<CaseEntity> _caseRepository;
    private readonly IGenericRepository<JournalEntity> _journalRepository;
    private readonly IGenericRepository<MessageEntity> _messageRepository;
    private readonly IGenericRepository<FileEntity> _fileRepository;
    private readonly INotificationSenderFactory _notificationSenderFactory;

    public MessagesBl(
      IGenericRepository<CaseEntity> caseRepository,
      IGenericRepository<JournalEntity> journalRepository,
      IGenericRepository<MessageEntity> messageRepository,
      IGenericRepository<FileEntity> fileRepository,
      INotificationSenderFactory notificationSenderFactory) {
      _caseRepository = caseRepository;
      _journalRepository = journalRepository;
      _messageRepository = messageRepository;
      _fileRepository = fileRepository;
      _notificationSenderFactory = notificationSenderFactory;
    }

    private Result<CaseEntity, ErrorResponse> RequireExistingCase(string caseId) =>
      _caseRepository.Get(caseId).OrServerError().MapValueOrNotFound();

    private Result<Maybe<MessageEntity>, ErrorResponse> GetMaybeMessage(string caseId, string messageId) =>
      RequireExistingCase(caseId).Bind(_ => _messageRepository.Get(messageId).OrServerError());

    private Result<MessageEntity, ErrorResponse> GetMessageInternal(string caseId, string messageId) =>
      GetMaybeMessage(caseId, messageId).MapValueOrNotFound();

    public Result<ChildrenListResponse, ErrorResponse> GetMessagesByCase(string caseId) =>
      RequireExistingCase(caseId)
        .Bind(_ => _messageRepository.GetAll(m => m.CaseId == caseId).OrServerError())
        .Map(m => m.Select(x => x.Id).ToList())
        .Map(m => ChildrenListResponse.Create(m, HttpStatusCode.OK));

    public Result<MessageResponse, ErrorResponse> GetMessage(string caseId, string messageId) =>
      GetMessageInternal(caseId, messageId).Map(m => MessageResponse.Create(m, HttpStatusCode.OK));

    public Result<SuccessResponse, ErrorResponse> CreateMessage(string caseId, string messageId, CreateMessageRequest request, string authorId = null) =>
      GetMaybeMessage(caseId, messageId)
        .Ensure(m => m.HasNoValue, ErrorResponse.Create(HttpStatusCode.Conflict, $"Message with Id {messageId} already exists"))
        .Bind(_ => _messageRepository
          .Add(new MessageEntity {
            Id = messageId,
            CaseId = caseId,
            Data = request.Data,
            AuthorId = authorId,
            CreationDate = DateTime.UtcNow,
            DataProcessingMethodHint = request.DataProcessingMethodHint
          })
          .MapError(e => ErrorResponse.Create(HttpStatusCode.InternalServerError, e)))
        .Tap(_ => _journalRepository.Add(new JournalEntity {Id = Guid.NewGuid().ToString(), CaseId = caseId, CreationDate = DateTime.UtcNow, JournalType = JournalEntryType.NewMessageByCreator}))
        .Tap(_ => _notificationSenderFactory
          .Create()
          .SendAsync(new Notification {CaseId = caseId, NotificationType = NotificationType.NewMessage, Message = $"A New Message for Case with Id {caseId} was created"})
          .GetAwaiter()
          .GetResult())
        .Map(_ => SuccessResponse.Create(HttpStatusCode.Created, "Message created"));

    public Result<FileContentResponse, ErrorResponse> GetFileContent(string caseId, string messageId, string fileId) =>
      GetMessage(caseId, messageId)
        .Bind(_ => _fileRepository.Get(fileId).OrServerError().MapValueOrNotFound())
        .Map(m => FileContentResponse.Create(m, HttpStatusCode.OK, "File found"));

    public Result<FileMetadataResponse, ErrorResponse> GetFileMetadata(string caseId, string messageId, string fileId) =>
      GetMessage(caseId, messageId)
        .Bind(_ => _fileRepository.Get(fileId).OrServerError().MapValueOrNotFound())
        .Map(m => FileMetadataResponse.Create(m, HttpStatusCode.OK, "File found"));

    public Result<SuccessResponse, ErrorResponse> DeleteFile(string caseId, string messageId, string fileId) =>
      GetMessage(caseId, messageId)
        .Bind(_ => _fileRepository.Remove(fileId)
          .Map(() => SuccessResponse.Create(HttpStatusCode.OK, "Deleted"))
          .OrServerError());

    public Result<SuccessResponse, ErrorResponse> UploadFile(string caseId, string messageId, string fileId, FileUploadRequest r) =>
      GetMessage(caseId, messageId)
        .Bind(_ => _fileRepository.Add(new FileEntity {
            CaseId = caseId, 
            Data = r.Data, 
            Id = fileId, 
            FileName = r.FileName, 
            MessageId = messageId, 
            UploadDate = DateTime.UtcNow, 
            UserId = null,
            DataProcessingMethodHint = r.DataProcessingMethodHint
          }).OrServerError()
          .Map(_ => SuccessResponse.Create(HttpStatusCode.OK, "Added")));

    public Result<ChildrenListResponse, ErrorResponse> GetFiles(string caseId, string messageId) =>
      GetMessage(caseId, messageId)
        .Bind(_ => _fileRepository.GetAll(f => f.MessageId == messageId)
          .Map(l => l.Select(f => f.Id).ToList())
          .Map(l => ChildrenListResponse.Create(l, HttpStatusCode.OK, "Found"))
          .OrServerError());
  }
}
