using System.Net;
using CSharpFunctionalExtensions;
using FluentAssertions;
using Reveil.Backend.Extensions;
using Reveil.Backend.Models.Api.Responses.Core;
using Xunit;

namespace Reveil.Backend.Tests.Extensions {
  public class HttpContextFunctionalExtensionsTests {
    [Fact]
    public void TestMatchAndSetStatusCode() {
      var r = Result.Success("Hello")
        .OrServerError()
        .Map(r => SuccessResponse.Create(HttpStatusCode.OK, r))
        .MatchAndSetStatusCode();

      r.IsSuccess.Should().BeTrue();
      r.StatusCode.Should().Be(HttpStatusCode.OK);
    }
  }
}
