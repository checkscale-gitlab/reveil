export interface IModelLocalesTCode {
    name: string,
    code: string,
}
