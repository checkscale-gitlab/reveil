// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Reveil.Backend.Models.Configuration.Notification {
  public class SmtpRecipientConfiguration {
    public string RecipientName { get; set; }
    public string RecipientEmail { get; set; }
  }
}
