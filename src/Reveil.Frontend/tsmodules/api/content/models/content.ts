import * as api from '~/api'
import { IModelContentPayload } from './payload'

export interface IModelContent {
    response: api.ContentEntryResponse | null,
    payload: IModelContentPayload,
}
